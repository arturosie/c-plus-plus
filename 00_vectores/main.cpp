#include <iostream>
#include <stdlib.h>
#include "string"
#include <math.h>

#define MAX 2

using namespace std;

void Calcula (double pos_now[MAX], double pos_target[MAX], double vel_max, double vel_actual[MAX]) {

    double vec_distancia[MAX] = {
        pos_target[0] - pos_now[0],
        pos_target[1] - pos_now[1]
    };

    double dis_total,
           dis_unitaria[MAX],
           vel_deseada[MAX],
           aceleracion[MAX];

           dis_total        = sqrt( pow(vec_distancia[0], 2) + pow(vec_distancia[1], 2) );
           dis_unitaria[0]  = vec_distancia[0] / dis_total;
           dis_unitaria[1]  = vec_distancia[1] / dis_total;
           vel_deseada[0]   = vel_max * dis_unitaria[0];
           vel_deseada[1]   = vel_max * dis_unitaria[1];
           aceleracion[0]   = vel_deseada[0] - vel_actual[0];
           aceleracion[1]   = vel_deseada[1] - vel_actual[1];

           cout <<"La aceleracion para llegar al objetiva es de (" << aceleracion[0] << "," << aceleracion[1] << ")";

}

void Datos (double pos_now[MAX], double pos_target[MAX]){

    double vel_max,
           vel_actual[MAX];

    system("clear");
    cout << "Introduce la posicion del avion actual en el eje X: " << endl;
    cin  >> pos_now[0];
    cout << "Introduce la posicion del avion actual en el eje Y: " << endl;
    cin  >> pos_now[1];
    cout << "Introduce la velocidad maxima del avion: " << endl;
    cin  >> vel_max;
    cout << "Introduce el objetivo anterior en el eje X (velocidad actual)" << endl;
    cin  >> vel_actual[0];
    cout << "Introduce el objetivo anterior en el eje Y (velocidad actual)" << endl;
    cin  >> vel_actual[1];
    cout << "Introduce la posicion del objetivo actual en el eje X: " << endl;
    cin  >> pos_target[0];
    cout << "Introduce la posicion del objetivo actual en el eje Y: " << endl;
    cin  >> pos_target[1];

    Calcula(pos_now, pos_target, vel_max, vel_actual);

}

void Menu (double pos_now[MAX], double pos_target[MAX]) {

    int option,
        x,
        y;

    system ("clear");
    cout << "1.- Buscar el objetivo" << endl;
    cout << "2.- Huir del objetivo"  << endl;
    cout << "3.- Salir \n"           << endl;
    cout << "Elige una opcion: ";
    cin  >> option;

    switch (option){
        case 1:
            Datos(pos_now, pos_target);

            break;
    }
}

    int
main (int argc, char *argv[])
{
    double pos_now [MAX],
    pos_target[MAX];

    Menu(pos_now, pos_target);
    cout << "\nTu avion esta en la posicion (" << pos_now[0] << "," << pos_now[1] << ")" << endl;
    cout << "\nTu objetivo esta en la posicion (" << pos_target[0] << "," << pos_target[1] << ")" << endl;
    return EXIT_SUCCESS;
}
